# Servidor Maratonei

**Progresso:** EM ANDAMENTO<br />
**Autor:** Paulo Victor de Oliveira Leal, Augusto de Castro Vieira, Vinicius Luiz Lopes Nunes<br />
**Data:** 2017<br />

## Objetivo
- Desenvolver um servidor capaz de calcular o Simplex para uma função com n variaveis de decisão e n restrições. O aplicativo comunica com o servidor (hospedado no Azure) através de requisições http, usando Json.

### Observação
- Visual Studio 2017 Enterprise
- NET Core 2.0
- Testado somente no Windows
IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://dotnet.microsoft.com/)<br />
Banco de dados: Não utiliza <br />

### Execução

    $ dotnet restore
    $ dotnet run

### Teste
- Para testar:
- Abra o navegador na url informada durante a compilação: 
    - http://localhost:.../
    - Se a pagina inicial for exibida o servidor está funcionando
- Abra o Postman e ultilize as requisições da coleção:
    - [PostmanCollection]  

### Distribuição dos arquivos

- Models:
    -  Estrutura da função objetiva (ObjectiveFunction.cs)
    -  Estrutura das restrições (Restrictio.cs)
-  Controller:
    -  Controle das requisições (SimplexController.cs)
    -  Components:
        -  Classe que efetua o calculo do Simplex (Simplex.cs)


### Contribuição

Esse projeto está finalizado e é livre para uso.
## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->

[PostmanCollection]: <https://www.getpostman.com/collections/48c7b85bb5c71c73a113>